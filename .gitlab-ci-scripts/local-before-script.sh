#!/bin/bash

echo "======== local-before-script starting======="

case ${DISTRO} in
    debian|ubuntu)
        make get-sources
    ;;
    win) # Do nothing for windows
    ;;
    *) # We expect only RPM by default
        set -x
        make get-sources
        make patch-for-rpm
        ls -la rpm
        set +x
    ;;
esac

echo "======== /local-before-script done   ========"
